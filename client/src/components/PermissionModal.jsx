import React, { useState } from 'react';
import { Modal, Card, Accordion, Row, Col, Button } from 'react-bootstrap';

import '../styles/PermissionModal.css';

import PermissionButton from './PermissionButton';

function PermissionModal({ user, updateUser, openModal }) {
  const [show, setShow] = useState(false);

  const handleModalShow = () => setShow(true);
  const handleModalClose = () => setShow(false);

  const togglePermission = (groupName, permName) => {
    const newGroups = user.groups.map((group) => {
      if (group.name === groupName) {
        const newPerms = group.permissions.map((permission) => {
          if (permission.name === permName) {
            return Object.assign(permission, { value: !permission.value });
          }
          return permission;
        });
        return Object.assign(group, { permissions: newPerms });
      }
      return group;
    });
    updateUser(Object.assign(user, { groups: newGroups }));
  };

  return (
    <>
      <Button
        onClick={handleModalShow}
        className="gradient"
        style={{ width: '6em' }}
      >
        <i className="fas fa-edit"></i>
      </Button>
      <Modal show={show} onHide={handleModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Update Permissions for {user.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Accordion>
            {user.groups.map((group, index) => (
              <Card key={index}>
                <Accordion.Toggle
                  as={Card.Header}
                  eventKey={index.toString()}
                  className="gradient"
                >
                  <Row>
                    <Col xs lg="1">
                      <i
                        style={{ fontSize: '1.5rem' }}
                        className={group.icon}
                      ></i>
                    </Col>
                    <Col style={{ userSelect: 'none', textAlign: 'center' }}>
                      {group.name}
                    </Col>
                    <Col xs lg="1">
                      <i
                        style={{ fontSize: '2rem' }}
                        className="fas fa-sort-down"
                      ></i>
                    </Col>
                  </Row>
                </Accordion.Toggle>
                <Accordion.Collapse eventKey={index.toString()}>
                  <Card.Body>
                    {group.permissions.map((permission, index) => (
                      <PermissionButton
                        permission={permission}
                        onClick={() =>
                          togglePermission(group.name, permission.name)
                        }
                        key={index}
                      />
                    ))}
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            ))}
          </Accordion>
        </Modal.Body>
      </Modal>
    </>
  );
}

export default PermissionModal;
